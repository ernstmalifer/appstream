var AppStream = angular.module('AppStream', ['ngRoute', 'ngDragDrop']);

AppStream
.controller('ContentsController', function ($scope) {

    $scope.menuboard = {};
    $scope.contents = {};
    
    $scope.getContents = function(){
        io.socket.get('/content', function (contents) {
            $scope.contents = contents;
            console.log($scope.contents);
            $scope.$apply();
        });
    }

    $scope.getMenuboard = function(menuboardid){
        io.socket.get('/menuboard/' + menuboardid, function (menuboard) {
            $scope.menuboard = menuboard;
            $scope.$apply();
        });
    }

    $scope.$on('contentLoaded', function(ngRepeatFinishedEvent) {
        $( ".draggable" ).draggable({revert: true});
    });

    $scope.$on('menuboardLoaded', function(ngRepeatFinishedEvent) {
        $( ".droppable" ).droppable({
          drop: function( event, ui ) {
            // console.log(this.id, ui.draggable.context.id);
            $scope.setToMenuboard(1, this.id, ui.draggable.context.id)
          }
        });
    });

    $scope.setToMenuboard = function(menuboard, zone, content){

        // console.log($scope.zonecontainers);
        var zone1 =  {"title":"Zone 1","posX":0,"posY":0,"width":960,"height":1080,"content": $scope.contents[content-1] };
        var zone2 =  {"title":"Zone 1","posX":960,"posY":0,"width":960,"height":1080,"content": $scope.contents[content-1] };

        io.socket.put('/menuboard/1',
            {
                "zones":[ zone1, zone2 ]
            },
            function (resData) {
                console.log(resData);
            }
        );
    }

    $scope.onStart = function(content){
        console.log('yow');
        // io.socket.put('/menuboard/1', {"zones":[{"title":"Zone 1","posX":0,"posY":0,"width":960,"height":1080,"content":{}},{"title":"Zone 1","posX":960,"posY":0,"width":960,"height":1080,"content":{}}]}, function (resData) {
        //   console.log(resData);
        // });
    }

    $scope.zonecontainers = [];

    $scope.getContents();
    $scope.getMenuboard(1);

});

AppStream
.controller('MenuboardController', function ($scope) {
    console.log('Menuboard');
});

AppStream
.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

AppStream
.config(function ($routeProvider) {
    $routeProvider.
    when('/', {
        templateUrl: 'templates/home.html'
    }).
    when('/content', {
        templateUrl: 'templates/content.html',
        controller: 'ContentsController'
    }).
    when('/menuboard', {
        templateUrl: 'templates/menuboard.html',
        controller: 'MenuboardController'
    }).
    otherwise({
        redirectTo: '/'
    });
});